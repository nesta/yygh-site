import request from '@/utils/request'

export default {
  getPageList(page, limit, searchObj) {
    return request({
      url: `/api/hosp/hospital/findHospList/${page}/${limit}`,
      method: 'get',
      params: searchObj
    })
  },

  getByHosname(hosname) {
    return request({
      url: `/api/hosp/hospital/findByHosname/${hosname}`,
      method: 'get'
    })
  },
  show(hoscode) {
    return request({
      url: `/api/hosp/hospital/findHospDetail/${hoscode}`,
      method: 'get'
    })
  },
  findDepartment(hoscode) {
    return request({
      url: `/api/hosp/hospital/department/${hoscode}`,
      method: 'get'
    })
  },
  //可预约的信息分页
  getBookingScheduleRule(page, limit, hoscode, depcode) {
    return request({
      url: `/api/hosp/hospital/auth/getBookingScheduleRule/${page}/${limit}/${hoscode}/${depcode}`,
      method: 'get'
    })
  },
  //获取排班信息
  findScheduleList(hoscode, depcode, workDate) {
    return request({
      url: `/api/hosp/hospital/auth/findScheduleList/${hoscode}/${depcode}/${workDate}`,
      method: 'get'
    })
  },
  //根据排班id获取排班信息
  getSchedule(id) {
    return request({
      url: `/api/hosp/hospital/getSchedule/${id}`,
      method: 'get'
    })
  },

}
