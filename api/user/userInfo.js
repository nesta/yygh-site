import request from '@/utils/request'

export default {
  login(userInfo) {
    return request({
      url: `/api/user/login`,
      method: `post`,
      data: userInfo
    })
  },
  getUserInfo() {
    return request({
      url: `/api/user/auth/getUserInfo`,
      method: `get`
    })
  },
  saveUserAuth(userAuah) {
    return request({
      url: `/api/user/auth/userAuth`,
      method: 'post',
      data: userAuah
    })
  }

}
