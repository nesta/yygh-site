import request from '@/utils/request'

export default {
  sendCode(mobile) {
    return request({
      url: `/api/msm/send/${mobile}`,
      method: `get`
    })
  }
}
